#!/usr/bin/python3
import sys, re

''' Create drop tables command from mysql tables input'''

# Read stdin
print('Reading from stdin\n++++++++++++++++++++++++\n')
tables = sys.stdin.readlines()
dropTables = []
for line in tables:

    # Remove title and ending line
    title = re.compile('^\|\s+Tables_in_.+')
    line = re.sub(title, ' ', line)

    # Remove all symbols
    notAlpha = re.compile('\W\d*')
    line = re.sub(notAlpha, ' ', line)

    # If line is not empty, format it and append to list
    if len(line.strip()) != 0:
        line = "drop table {};".format(line.strip())
        dropTables.append(line)

for table in dropTables:
    print(table)
