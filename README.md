# dropTables

This is a simple yet very useful __python3__ script to help you drop mysql
tables from a database.

### Usage

In your mysql console use the command `show tables` to list all the tables in
the database.  Copy all the tables you want to drop from the database and paste
it in a file.  This script reads from __STDIN__ so you can pipe your file to the
script like this:

`cat tables_to_drop | ./dropTables.py`

The resulting command to drop the tables will be output to STDOUT. You can pipe
it to a file (using `>>` to append each line to the file) or copy and paste it
straight into your mysql cli.
